edk2 (0~20180626.e5735040-2) UNRELEASED; urgency=medium

  * debian/control: Point the Vcs-* links to the new location on salsa.

 -- dann frazier <dannf@debian.org>  Wed, 27 Jun 2018 16:45:06 -0600

edk2 (0~20180626.e5735040-1) unstable; urgency=medium

  * New upstream release.
  * d/p/ShellPkg-dp-Correct-case-of-included-file.patch: Add; fixes FTBFS.
  * debian/control: Point the Vcs-* links to the edk2 project in my
    namespace on salsa until we identify a more permanent location.

 -- dann frazier <dannf@debian.org>  Tue, 26 Jun 2018 17:06:59 -0600

edk2 (0~20180503.ebafede9-1) unstable; urgency=medium

  * New upstream release.

 -- dann frazier <dannf@debian.org>  Thu, 03 May 2018 17:00:35 -0600

edk2 (0~20180328.c27c0003-1) unstable; urgency=medium

  * New upstream release.
  * Bump openssl up to latest upstream version, 1.1.0h.

 -- dann frazier <dannf@debian.org>  Wed, 28 Mar 2018 11:06:08 -0600

edk2 (0~20180205.c0d9813c-2) unstable; urgency=medium

  * Enable HTTP Boot. LP: #1750481.

 -- dann frazier <dannf@debian.org>  Tue, 20 Feb 2018 13:14:10 -0700

edk2 (0~20180205.c0d9813c-1) unstable; urgency=medium

  * New upstream release.

 -- dann frazier <dannf@debian.org>  Mon, 05 Feb 2018 12:03:01 -0700

edk2 (0~20180105.0bc94c74-1) unstable; urgency=medium

  * New upstream release.
    - d/p/Revert-BaseTools-Update-Gensec-to-set-PROCESSING_REQ.patch: Drop;
      superseded by upstream fix:
       1e6e6e18 BaseTools: Fix GenSec GCC make failure
  * Bump Standards-Version from 4.1.1 to 4.1.3.
    - Use https instead of http in Vcs-Browser URL.

 -- dann frazier <dannf@debian.org>  Fri, 05 Jan 2018 12:33:43 -0700

edk2 (0~20171205.a9212288-1) unstable; urgency=medium

  * New upstream release.
    - Fix Windows Server 2012 BSOD during installation. Closes: #881219.
      Thanks to Jeff Ketchum.
    - Bump openssl up to latest upstream version, 1.1.0g.
  * d/p/Revert-BaseTools-Update-Gensec-to-set-PROCESSING_REQ.patch: Add;
    fixes FTBFS.
  * Change package priorities from extra (now deprecated) to optional.

 -- dann frazier <dannf@debian.org>  Tue, 05 Dec 2017 15:04:06 -0700

edk2 (0~20171027.76fd5a66-1) unstable; urgency=medium

  * New upstream release.
    - Fix Win10 guests booting from IDE drives. LP: #1725560.

 -- dann frazier <dannf@debian.org>  Fri, 27 Oct 2017 16:10:29 -0600

edk2 (0~20171010.234dbcef-1) unstable; urgency=medium

  * New upstream release.
    - d/p/demote-maybe-uninitialized-to-warning.patch: Drop; issue resolved
      upstream.
  * Bump Standards-Version from 3.9.8 to 4.1.1.
  * Bump debhelper compatibility level to 10.

 -- dann frazier <dannf@debian.org>  Tue, 10 Oct 2017 14:28:01 -0600

edk2 (0~20170911.5dfba97c-1) unstable; urgency=medium

  * New upstream release.
    - Now builds with gcc-7. Closes: #853382.
    - d/p/no-missing-braces.diff: Refresh.
    - d/p/no-stack-protector-all-archs.diff: Refresh.
    - d/p/no-pie-for-arm.diff: Drop; superseded by upstream commit
      a6b53806.
    - OpenSSL: Switch to the new openssl-1.1-based system, which no
      longer requires patching.
    - d/p/demote-maybe-uninitialized-to-warning.patch: Workaround compiler
      error until upstream code is fixed.
  * Unset environment variables that are used internally by edk2.
  * Avoid the need for "post-patches" by explicitly setting the
    ACTIVE_PLATFORM and TARGET_ARCH variables on the build commandline
    for ovmf, like we already do for qemu-efi-{arm,aarch64}.

 -- dann frazier <dannf@debian.org>  Tue, 12 Sep 2017 13:17:42 -0600

edk2 (0~20161202.7bbe0b3e-2) experimental; urgency=medium

  * debian/rules: Replace hardcoded "AARCH64" strings with $(EDK2_HOST_ARCH).
  * debian/rules: AAVMF image generation doesn't require the edksetup
    environment, so move that code outside of it.
  * debian/rules: Refactor build-qemu-efi into common and aarch64-specific
    targets, so that the common target can be used by a future arm-specific
    target.
  * d/p/arm64-no-pie-for-you.diff -> d/p/no-pie-for-arm.diff: Update patch
    to also apply to arm builds.
  * Rename qemu-efi to qemu-efi-aarch64 to open the namespace for
    qemu-efi-arm. qemu-efi is now a transitional package with a compatibility
    symlink.
  * Add qemu-efi-arm package. Closes: #857858.

 -- dann frazier <dannf@debian.org>  Thu, 18 May 2017 10:17:41 -0600

edk2 (0~20161202.7bbe0b3e-1) unstable; urgency=medium

  * New upstream release.
    - GOP driver for the VirtIo GPU (virtio-gpu-pci). Closes: #842680.
    - Drop precompiled binaries from Vlv2TbltDevicePkg/.
    - Drop precompiled liblto-*.a binaries from ArmPkg/.
  * Add myself to Uploaders.
  * debian/rules: Set OpenSSL version in one place.
  * d/p/arm64-reorder-blocks-algorithm.diff: Drop; superseded by
    upstream commit 8866d337.
  * d/p/arm64-no-pie-for-you.diff: Fix FTBFS w/ GCC that has PIE
    enabled by default (now the case in Debian). Closes: #846690.
  * debian/control: Clarify the package/guest architecture mapping.
    Closes: #837092.
  * d/p/no-missing-braces.diff: Refresh.
  * d/p/no-stack-protector-all-archs.diff: Refresh.
  * debian/copyright: Update.

 -- dann frazier <dannf@debian.org>  Fri, 09 Dec 2016 09:09:39 +0100

edk2 (0~20160813.de74668f-2) unstable; urgency=medium

  [ dann frazier ]
  * d/p/arm64-reorder-blocks-algorithm.diff: Workaround to fix
    booting in KVM mode. LP: #1632875.
  * debian/rules: Export compiler prefix variable to simplify
    build-qemu-efi target.
  * debian/rules: Make the target dependencies on setup-build explicit.
  * debian/rules: Set the aarch64 toolchain prefix agnostically of the
    toolchain tag being used.
  * debian/rules: Respect EDK2_TOOLCHAIN tag when building ovmf.

 -- Steve Langasek <vorlon@debian.org>  Wed, 19 Oct 2016 13:07:08 -0700

edk2 (0~20160813.de74668f-1) unstable; urgency=medium

  * New upstream release.
    - fixes build failure with gcc-6.  Closes: #834467.
    - increases variable size for arm64 build, to support SecureBoot.
      Closes: #830488.
  * debian/patches/shell-proper-valist.patch: use VA_COPY() in Shell.
  * update Standards Version.

 -- Steve Langasek <vorlon@debian.org>  Tue, 16 Aug 2016 06:20:28 +0000

edk2 (0~20160408.ffea0a2c-2) unstable; urgency=medium

  * Provide split AAVMF_{CODE,VARS}.fd for arm64 in the qemu-efi package,
    for VM-friendly nvram persistence in the same style as Fedora et al.
    and by analogy with the OVMF_{CODE,VARS}.fd on x86.  Thanks to
    William Grant <wgrant@ubuntu.com> for the patch.

 -- Steve Langasek <vorlon@debian.org>  Sat, 16 Apr 2016 00:30:50 +0000

edk2 (0~20160408.ffea0a2c-1) unstable; urgency=medium

  [ dann frazier ]
  * New upstream version.
    - d/p/enable-nvme: Drop; superseded by upstream commit 8ae3832d.
    - d/p/no-missing-braces.diff: Refresh.
    - d/p/FatPkg-AARCH64.diff: Drop; fixed upstream in commit 04a4fdb9.
    - d/p/no-stack-protector-all-archs.diff: Refresh.
    - d/p/arm64-mistrict-align.patch: Drop; superseded by upstream
      commit d764d5984.
  * Move out of non-free as the FAT driver has been replaced with a free
    implementation, Thanks to Microsoft.  Closes: #815618, LP: #1569602.
  * Add SECURE_BOOT_ENABLE flag to aarch64 build to enable support for UEFI
    Secure Boot.  Closes: #819757. Thanks to Linn Crosetto.

 -- Steve Langasek <vorlon@debian.org>  Thu, 14 Apr 2016 20:50:11 +0000

edk2 (0~20160104.c2a892d7-1) unstable; urgency=medium

  * New upstream version.
    - Fixes support for kvm GPU passthrough.  Closes: #810163.
    - Adds GICv3 support.  Closes: #810495.

  [ dann frazier ]
  * Use GCC49 toolchain for all architectures; the ARMGCC toolchain has
    been dropped upstream.
  * Supersede debian/patches/arm64-no-expensive-optimizations.patch
    with debian/patches/arm64-mstrict-align.patch.  Closes LP: #1489460.

 -- Steve Langasek <vorlon@debian.org>  Thu, 28 Jan 2016 01:35:30 +0000

edk2 (0~20150106.5c2d456b-2) unstable; urgency=medium

  [ Steve Langasek ]
  * Build-depend on gcc-aarch64-linux-gnu and make qemu-efi an Arch: all
    package.
  * Ship OVMF_CODE.fd and OVMF_VARS.fd for proper EFI variable support.
    Closes: #764918.  Continue shipping OVMF.fd too for now, for
    compatibility.

  [ dann frazier ]
  * qemu-efi: Switch to Intel BDS. This supports a fallback to the removable
    media path (i.e. \EFI\BOOT\BOOTaa64.EFI) as required by the Linaro VM
    Specification.  Closes: #796928.
  * debian/patches/arm64-no-expensive-optimizations.patch: Workaround
    ARM64 compiler issue by disabling certain optimizations.
    Closes: LP #1489560

 -- Steve Langasek <vorlon@debian.org>  Thu, 03 Sep 2015 22:08:41 +0000

edk2 (0~20150106.5c2d456b-1) unstable; urgency=medium

  * New upstream release, for arm64 support.
  * debian/patches/no-missing-braces.diff: Add -Wno-missing-braces to
    CFLAGS to avoid build failures.  Thanks to dann frazier
    <dannf@debian.org>.
  * debian/patches/FatPkg-AARCH64.diff: AARCH64 support.  Thanks to dann
    frazier <dannf@debian.org>.
  * Drop debian/patches/fix-undefined-behavior-in-vfrcompiler.patch, included
    upstream.
  * Drop debian/patches/gcc-4.9-align.patch in favor of using the GCC49
    upstream toolchain rules.
  * Adjust debian/rules to only build ovmf when building with -b, in
    preparation for enabling other architecture builds (which currently can't
    be Arch: all due to lack of cross-compilers in the Debian archive).

  [ dann frazier ]
  * Add new qemu-efi package for arm64.  Closes: #775308.

  [ Steve Langasek ]
  * Refactor debian/rules to support cross-building.
  * debian/patches/no-stack-protector-all-archs.diff: pass
    -fno-stack-protector to all ARM GCC toolchains.
  * Add XS-Build-Indep-Architecture to debian/control, as a temporary 
    measure pending standardization, to work around Launchpad builder
    behavior which would try to build our arch: all package on an arm64
    builder instead of an x86 one.
  * Fix Vcs-Git URI in debian/control.
  * Standards-Version 3.9.6.

 -- Steve Langasek <vorlon@debian.org>  Thu, 05 Feb 2015 14:57:40 +0000

edk2 (0~20131112.2590861a-3) unstable; urgency=medium

  [ Steve Langasek ]
  * debian/copyright: include a Disclaimer field to document clearly why
    this package is not in main.  Closes: #742589.

  [ Michael Tokarev ]
  * apply gcc-4.9-align.patch kindly provided by dann frazier to fix ftbfs
    with gcc-4.9 (Closes: #771114)
  * apply upstream fix-undefined-behavior-in-vfrcompiler.patch, kindly provided
    by dann frazier, to fix another ftbfs (Closes: #773492)

 -- Michael Tokarev <mjt@tls.msk.ru>  Fri, 19 Dec 2014 10:16:14 +0300

edk2 (0~20131112.2590861a-2) unstable; urgency=medium

  * debian/ovmf.links: create a OVMF.fd link for qemu
  * debian/control: ovmf Replaces qemu-system-common versions which
    shipped that link in Ubuntu.

 -- Serge Hallyn <serge.hallyn@ubuntu.com>  Tue, 25 Feb 2014 09:50:04 -0600

edk2 (0~20131112.2590861a-1) unstable; urgency=medium

  * New upstream release, requested by Dimitri Ledkov for persistent nvram
    variable support.
  * Pass -DFD_SIZE_2MB to the build, since we're now over the size limit

 -- Steve Langasek <vorlon@debian.org>  Thu, 30 Jan 2014 11:47:05 +0000

edk2 (0~20131029.2f34e065-1) unstable; urgency=medium

  * New upstream release.  Closes: #714463.
    - update debian/rules to pull a new version of the shell.
    - drop debian/patches/enum-handling, fixed upstream.
    - drop debian/patches/mismatched-enums, fixed upstream.
    - fixes breakage with the EFI shell.  LP: #1223413.
  * debian/patches/enable-nvme: enable the NVMe driver.
    Closes LP: #1267816.
  * debian/post-patches/setup.diff: drop gcc4.7 handling, which is
    sorted upstream.
  * Update debian/copyright

 -- Steve Langasek <vorlon@debian.org>  Sat, 11 Jan 2014 23:34:25 +0000

edk2 (0~20121205.edae8d2d-2) unstable; urgency=low

  * Fix the package section and debian/copyright: the FAT driver has a
    license addendum which makes it non-free instead of BSD.
    Closes: #714322.
  * Make our build friendlier to git checkouts, by making sure our target
    dir exists before copying.

 -- Steve Langasek <vorlon@debian.org>  Wed, 25 Sep 2013 03:35:20 +0000

edk2 (0~20121205.edae8d2d-1) unstable; urgency=low

  * Initial release.

 -- Steve Langasek <vorlon@debian.org>  Sun, 10 Feb 2013 06:45:06 +0000
